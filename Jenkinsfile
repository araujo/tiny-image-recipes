release = "17.12"

properties(
    [
        pipelineTriggers([cron('H H(1-4) * * *')]),
    ]
)

def uploadDirectory(source, target, upload = true) {

  if (!upload) {
    println "Skipping upload of ${source} to ${target}"
    return
  }

  sshagent (credentials: [ "5a23cd79-e26d-41bf-9f91-d756c131b811", ] ) {
    env.NSS_WRAPPER_PASSWD = "/tmp/passwd"
    env.NSS_WRAPPER_GROUP = '/dev/null'
    sh(script: 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > ${NSS_WRAPPER_PASSWD}')
    sh(script: "LD_PRELOAD=libnss_wrapper.so rsync -e \"ssh -oStrictHostKeyChecking=no\" -aP ${source} archive@images.apertis.org:/srv/images/internal/${target}/")
  }
}

def runTestsJobs(version, release, type, arch, board, submit = true) {

  if (!submit) {
    println "Skipping submitting tests jobs"
    return
  }

  dir ("apertis-tests") {
    git(url: "https://git.apertis.org/cgit/apertis-tests.git/",
        poll: false,
        branch: "master")
  }

  // TODO: Remove "uefi" once images run in the actual boards
  def boardp = (board in ["uefi", "sdk"]) ? "qemu" : board

  withCredentials([ file(credentialsId: 'apertis-lava-user', variable: 'lqaconfig') ]) {
    sh(script: """\
    /usr/bin/lqa -c ${lqaconfig} submit \
    --profile apertis-${release}-${type}-${arch}-${boardp} \
    -g apertis-tests/templates/profiles.yaml \
    -t image_date:${version}""")
  }

}

def buildImage(architecture, type, board, debosarguments = "", production = false) {
  return {
    node("docker-slave") {
      checkout scm
      docker.withRegistry('https://docker-registry.apertis.org') {
        buildenv = docker.image("docker-registry.apertis.org/apertis/apertis-${release}-image-builder")
        /* Pull explicitly to ensure we have the latest */
        buildenv.pull()

        buildenv.inside("--device=/dev/kvm") {
          stage("setup ${architecture} ${type}") {
            env.PIPELINE_VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
            sh ("env ; mkdir -p ${PIPELINE_VERSION}/${architecture}/${type}")
          }

          try {
              stage("${architecture} ${type} connectivity container") {
                   sh(script: """\
                   cd ${PIPELINE_VERSION}/${architecture}/${type}; \
                   debos ${debosarguments} \
                    -t type:${type} \
                    -t architecture:${architecture} \
                    -t suite:${release} \
                    -t timestamp:${PIPELINE_VERSION} \
                    -t ospack:tiny-connectivity-container_${release}-${architecture}-${type}_${PIPELINE_VERSION}.tar.gz \
                    ${WORKSPACE}/tiny-connectivity-container.yaml""")
              }

              stage("${architecture} ${type} connectivity container upload") {
                uploadDirectory (env.PIPELINE_VERSION, "tiny/${release}", production)
              }

              stage("Submitting tests jobs (lava)") {
                runTestsJobs (env.PIPELINE_VERSION, release, type, architecture, board, false)
              }

          } finally {
            stage("Cleanup ${architecture} ${type}") {
              deleteDir()
            }
          }
        }
      }
    }
  }
}


/* Determine whether to run uploads based on the prefix of the job name; in
 * case of apertis we expect the official jobs under apertis-<release>/ while
 * non-official onces can be in e.g. playground/ */
def production = env.JOB_NAME.startsWith("apertis-")

def images = [:]

// Types for all boards, common debos arguments
def  types = [ [ "connectivity", "" ],
            ]

images += types.collectEntries { [ "Amd64 ${it[0]}": buildImage("amd64",
                                   it[0],
                                   "uefi",
                                   it[1],
                                   production ) ] }

images += types.collectEntries { [ "Arm64 ${it[0]}": buildImage("arm64",
                                   it[0],
                                   "uboot",
                                   it[1],
                                   production ) ] }

images += types.collectEntries { [ "Armhf ${it[0]}": buildImage("armhf",
                                   it[0],
                                   "uboot",
                                   it[1],
                                   production ) ] }

parallel images
