LXC template generates configuration file for container, pre-mount hook and AppArmor profile.
Template is compatible with LXC upstream, so tools from LXC should be used to create/start/stop/destroy the container.

# Create container:

Image to be used as the container can either be downloaded from a URL or built using the instructions found in the parent folder.
This example uses the instructions, which generates `ospack_18.03-amd64_00000000.0.tar.gz`.

  sudo lxc-create -t $PWD/lxc-tiny-connectivity --name tiny-container -- --ospack "$PWD/../ospack_18.03-amd64_00000000.0.tar.gz"

Options:
 * -t : Use template named `lxc-tiny-connectivity` from current directory
 * -P : Use an alternate container path. The default is /var/lib/lxc
 * --name : Name of container
 * -- (separator) : Options for template must be added after this separator
 * --ospack : URL to download or path to the image

# Start the container in the background:

  sudo lxc-start --name tiny-container

# Start the container in foreground mode:

  sudo lxc-start -F --name tiny-container

Pull the ostree, deploy and reboot into deployed OS tree:

  sudo ostree admin upgrade -r

# Stop the container:

  sudo lxc-stop --name tiny-container

# Destroy the container and associated configuration:

  sudo lxc-destroy --name tiny-container
