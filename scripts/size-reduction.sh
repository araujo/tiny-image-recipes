#!/bin/sh
set -ve

# Crush into a minimal production image to be deployed via some type of image
# updating system. The Debian system is not longer functional at this point.

UNNEEDED_PACKAGES="adduser
apertis-archive-keyring
apt
apt-transport-https
ca-certificates
debconf
diffutils
e2fslibs
e2fsprogs
gcc-5-base
gcc-6-base
gnupg
gpgv
gzip
hostname
insserv
iptables
libapparmor-perl
libapt-pkg5.0
libasn1-8-heimdal
libaudit-common
libbsd0
libbz2-1.0
libcomerr2
libcryptsetup4
libcurl3
libdb5.3
libdebconfclient0
libdevmapper1.02.1
libedit2
libffi6
libfuse2
libgmp3c2
libgnutls30
libgssapi3-heimdal
libgssapi-krb5-2
libhcrypto4-heimdal
libheimbase1-heimdal
libheimntlm0-heimdal
libhogweed4
libhx509-5-heimdal
libk5crypto3
libkeyutils1
libkrb5-26-heimdal
libkrb5-3
libkrb5support0
libldap-2.4-2
liblocale-gettext-perl
libncurses5
libncursesw5
libnettle6
libnfnetlink0
libp11-kit0
libpam-modules
libpam-modules-bin
libprocps4
libreadline5
libroken18-heimdal
librtmp1
libsasl2-2
libsasl2-modules-db
libsemanage1
libsemanage-common
libsepol1
libsmartcols1
libsqlite3-0
libss2
libssl1.0.0
libtasn1-6
libtext-charwidth-perl
libtext-iconv-perl
libtext-wrapi18n-perl
libtinfo5
libudev1
libusb-0.1-4
libustr-1.0-1
libwind0-heimdal
libwrap0
libxtables11
login
lsb-base
lzma
mawk
mktemp
mount
multiarch-support
ncurses-bin
ncurses-base
net-tools
openssh-client
openssh-server
openssh-sftp-server
openssl
passwd
perl-base
procps
sed
sensible-utils
sysv-rc
tzdata
util-linux
vim-common
vim-tiny
vim-tiny-dbgsym
xz-utils
zlib1g "

UNNEEDED_DIRS="var/* 
opt
srv
usr/share/locale
usr/share/man
usr/share/doc
usr/share/ca-certificates
usr/share/bash-completion
usr/share/zsh/vendor-completions
usr/share/gcc-5
etc/init.d
etc/rc[0-6S].d
etc/init
usr/lib/xtables
usr/lib/locale/*
usr/lib/systemd/boot
usr/lib/systemd/catalog
usr/lib/lsb "

UNNEEDED_FILES="usr/sbin/visudo
usr/sbin/useradd
usr/sbin/usermod
usr/sbin/chpasswd
usr/sbin/*fdisk
usr/bin/localedef
usr/lib/iptables/iptables.init
usr/bin/pg
usr/bin/watch
usr/bin/slabtop
usr/games
lib/systemd/system-generators/systemd-cryptsetup-generator
lib/systemd/system-generators/systemd-debug-generator
lib/systemd/system-generators/systemd-gpt-auto-generator
lib/systemd/system-generators/systemd-hibernate-resume-generator
lib/systemd/system-generators/systemd-rc-local-generator
lib/systemd/system-generators/systemd-system-update-generator
lib/systemd/system-generators/systemd-sysv-generator
usr/bin/bootctl
usr/bin/busctl
usr/bin/hostnamectl
usr/bin/localectl
usr/bin/systemd-cat
usr/bin/systemd-cgls
usr/bin/systemd-cgtop
usr/bin/systemd-delta
usr/bin/systemd-detect-virt
usr/bin/systemd-mount
usr/bin/systemd-path
usr/bin/systemd-run
usr/bin/systemd-socket-activate "

# Removing unused packages
for PACKAGE in ${UNNEEDED_PACKAGES}
do
	echo "Forcing removal of ${PACKAGE}"
	if ! dpkg --purge --force-remove-essential --force-depends "${PACKAGE}"
	then
		echo "WARNING: Failed to purge ${PACKAGE}"
	fi
done

# Show what's left package-wise before dropping dpkg itself
COLUMNS=300 dpkg -l
SIZES=`dpkg-query -Wf '${Installed-Size}\t${Package}\n' | sort -n`

# Drop dpkg
dpkg --purge --force-remove-essential --force-depends  dpkg

# Removing unused directories
for DIR in ${UNNEEDED_DIRS}
do
	echo "Forcing removal of ${DIR}"
	if ! rm -rf "${DIR}"
	then
		echo "WARNING: Failed to remove ${DIR}"
	fi
done

# Removing unused files
for FILE in ${UNNEEDED_FILES}
do
	echo "Forcing removal of ${FILE}"
	if ! rm -f "${FILE}"
	then
		echo "WARNING: Failed to remove ${FILE}"
	fi
done

echo "Find and remove systemd hw database manager and fuse files"
find usr etc -name '*systemd-hwdb*' -prune -exec rm -r {} \;
find usr etc -name '*fuse*' -prune -exec rm -r {} \;

echo "== Size and package list"
echo
echo $SIZES
